//
//  AppDelegate.swift
//  ATQueue
//
//  Created by Dejan on 14/08/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        testQueue()
        
        return true
    }
    
    private func testQueue() {
        let queue = ATQueue<Int>()
        queue.enqueue(1)
        queue.enqueue(2)
        queue.enqueue(15)
        queue.enqueue(11)
        queue.enqueue(3)
        
        print("size: \(queue.size)")
        print("elements: \(queue.elements)")
        print("reversed elements: \(queue.reversedElements)")
        
        let item = queue.dequeue()
        print("dequeued item: \(item)")
        
        print("size: \(queue.size)")
        print("elements: \(queue.elements)")
        print("reversed elements: \(queue.reversedElements)")
        
        print("contains 15? - \(queue.contains(15))")
        
        let secondItem = queue.dequeue()
        print("dequeued item: \(secondItem)")
        
        print("size: \(queue.size)")
        print("elements: \(queue.elements)")
        print("reversed elements: \(queue.reversedElements)")
    }
}
